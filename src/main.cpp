// my first program in C++
#include "../lib/noeud.hpp"
#include "../lib/graphe.hpp"
#include "../lib/agent.hpp"
#include "../lib/map.hpp"
#include "../lib/parcourgraphe.hpp"

#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream>
#include <vector>

#include "../lib/graphics/texture.hpp"
#include "../lib/graphics/sprite.hpp"
#include "../lib/graphics/graphicMap.hpp"
#include "../lib/graphics/labyrintheMap.hpp"
#include "../lib/graphics/objectRender.hpp"
#include "../lib/graphics/player.hpp"
#include "../lib/graphics/graphicNode.hpp"


int main(int argc, char* argv[]){
	char* parcours = "BestFirst";
	char* cheminL = "src/labyrinthe1.map";
	//test des argument
	if (argc >1){
		parcours = argv[1];
	}
	if (argc >2){
		cheminL = argv[2];
	}

	std::string typeParcours(parcours);
	Noeud noeud(0,0);

	std::string fichier_labyrinthe(cheminL);
	Map map(fichier_labyrinthe);
	//map.AfficherConsole();

	std::cout <<"Graphe"<<std::endl;
	Graphe graphe(map.getStringMap());
	Parcourgraphe parcourgraphe;
	parcourgraphe.initParcour(typeParcours, graphe, graphe.sortie, graphe.graphe[1]);
	int status = 1;
	std::vector<Noeud*> chemin;
	int spriteSize = 16;
	Texture * nodeTexture = new Texture("src/textures/dirt.png","node",32,32);

	//Création d'un agent
	Agent agent(graphe.graphe[1], "A*", &graphe);

	//liste des objets de la scene
	std::vector<ObjectRender*> objets;
	std::vector<GraphicNode*> noeudGraphiques;

	//creation des objets
    LabyrintheMap lmap = LabyrintheMap(fichier_labyrinthe,spriteSize);
    Player player = Player(1,1,spriteSize);
    objets.push_back(&lmap);
    //objets.push_back(&player);

    //construction de la fenetre
    sf::RenderWindow window(sf::VideoMode(lmap.getWidth(), lmap.getHeight()), "Labyrinthe");

    int posx = 0;

		int iterateur_chemin = 0;
    //boucle de la fenetre
    while (window.isOpen())
    {
				//parametre fenetre
        sf::Event event;
        while (window.pollEvent(event)){
            if (event.type == sf::Event::Closed)
                window.close();
        }

				//clear screen
        window.clear();

				//affichage des objets de la scene
        for(int i = 0; i < objets.size(); ++i){
          objets.at(i)->render(window);
        }

		for(int i = 0; i < noeudGraphiques.size(); ++i){
			noeudGraphiques.at(i)->render(window);
        }
		//affichage de player
		player.render(window);
		//affichage
        window.display();
		//sleep
        sf::sleep(sf::seconds(0.1));

		if(status == 1){
			status = parcourgraphe.pas();
			noeudGraphiques.clear();
			for(int i = 0 ; i < parcourgraphe.openList.size(); i++){
				GraphicNode * graphicNode = new GraphicNode(parcourgraphe.openList.at(i)->y,parcourgraphe.openList.at(i)->x,sf::Color(0,0,255),spriteSize,nodeTexture);
				noeudGraphiques.push_back(graphicNode);
			}
			for(int i = 0 ; i < parcourgraphe.closedList.size(); i++){
				GraphicNode * graphicNode = new GraphicNode(parcourgraphe.closedList.at(i)->y,parcourgraphe.closedList.at(i)->x,sf::Color(255,0,0),spriteSize,nodeTexture);
				noeudGraphiques.push_back(graphicNode);
			}
        }

		if(status == 0){
			if(chemin.size()==0){
				chemin = parcourgraphe.reconstituerChemin(graphe.sortie);
			}else
			{
				if(iterateur_chemin < chemin.size()){
					GraphicNode * graphicNode = new GraphicNode(chemin.at(iterateur_chemin)->y,chemin.at(iterateur_chemin)->x,sf::Color(0,255,0),spriteSize,nodeTexture);
					noeudGraphiques.push_back(graphicNode);
					iterateur_chemin ++;
				}
			}
		}
		if(agent.noeudCourant != graphe.sortie){
			agent.bouger();
			player.setPosition(agent.noeudCourant->y,agent.noeudCourant->x);
		}
    }
    return 0;
}
