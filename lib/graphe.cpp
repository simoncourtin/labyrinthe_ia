#include "graphe.hpp"
#include "noeud.hpp"

//* classe Graphe
void Graphe::parcourirGraphe(Noeud n){

}

Graphe::Graphe(){

}

Graphe::Graphe(std::vector<std::string> v){
  for(int i = 0; i < v.size(); i++){
    for(int j = 0 ; j < v.at(i).size(); j++){
      //si l'element est autre chose qu'un mur
      if(v.at(i).at(j) != '1' ){
        //selon l'element
        Noeud* noeud = new Noeud(i,j);
        this->graphe.push_back(noeud);
        if (v.at(i).at(j) == 'S'){
            this->sortie = noeud;
        }
      }
    }
  }
  
  //calculer les voisins
  for (int i = 0; i< this->graphe.size(); i++){
    for (int j = 0; j< this->graphe.size(); j++){
      if (this->graphe[i]->x == this->graphe[j]->x && (this->graphe[i]->y == this->graphe[j]->y-1 || this->graphe[i]->y == this->graphe[j]->y+1 )){
        this->graphe[i]->addVoisin(this->graphe[j]);
      }
      if (this->graphe[i]->y == this->graphe[j]->y && (this->graphe[i]->x == this->graphe[j]->x-1 || this->graphe[i]->x == this->graphe[j]->x+1 )){
        this->graphe[i]->addVoisin(this->graphe[j]);
      }
    }
  }
}

//*  fonction de lecture
std::vector<std::string> lireFichierGraphe(std::string nom_fichier){
  std::ifstream file;
  std::vector<std::string> map;

  file.open(nom_fichier.c_str());

  std::string line;
  if (file.is_open()) {
    while (std::getline(file, line)) {
      map.push_back(line);
    }
  }
  file.close();
  return map;
}
