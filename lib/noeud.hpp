#ifndef NOEUD
#define NOEUD

#include <iostream>
#include <list>
#include <vector>

class Noeud{
	public:
	    Noeud(int x, int y);
	    void addVoisin(Noeud* noeud);
		int x;
		int y;
		std::vector<Noeud*> voisin;
		int cout;
		float heuristique;
};

#endif