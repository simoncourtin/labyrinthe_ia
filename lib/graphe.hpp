#ifndef GRAPHE
#define GRAPHE

#include <iostream>
#include <fstream>
#include "noeud.hpp"
#include <vector>

class Graphe{
	public :
		void parcourirGraphe(Noeud n);
		Graphe(std::vector<std::string> v);
		Graphe();
		std::vector<Noeud*> graphe;	
		Noeud* sortie;	
};


std::vector<std::string>  lireFichierGraphe(std::string nom_fichier);

#endif
