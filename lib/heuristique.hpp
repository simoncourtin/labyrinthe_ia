#ifndef HEURISTIQUE
#define HEURISTIQUE
#include "agent.hpp"

typedef Noeud* (*Heuristique)(Noeud*, Noeud*,Noeud*);

Heuristique getHeuristique(std::string heuristique);

Noeud* AStar(Noeud* courant, Noeud* precedent, Noeud* sortie);

Noeud* BestFirst(Noeud* courant, Noeud* precedent, Noeud* sortie);

Noeud* CoutUniforme(Noeud* courant, Noeud* precedent, Noeud* sortie);

#endif