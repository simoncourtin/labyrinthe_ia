#include "map.hpp"

//* classe Map
Map::Map(std::string fichier_map){
  this->fichier_map = fichier_map;
  this->width = 0;
  this->height = 0;
  lireFichierMap(fichier_map);
}

//*  fonction de lecture
std::vector<std::string> Map::lireFichierMap(std::string nom_fichier){

  std::ifstream file;
  std::vector<std::string> map;

  this->width = 0;
  this->height = 0;

  file.open(nom_fichier.c_str());

  std::string line;
  if (file.is_open()) {
    while (std::getline(file, line)) {
      if(line.length() > this->width){
         this->width = line.length();
       }
      map.push_back(line);
    }
  }
  file.close();

  this->string_map = map ;
  this->height = map.size();
  return map;
}

void Map::AfficherConsole(){
  //parcours de la martice fichier
  for(int i = 0; i < this->string_map.size(); i++){
    for(int j = 0 ; j < this->string_map.at(i).size(); j++){
      //selon l'element
      switch(this->string_map.at(i).at(j)) {
        case '0' : std::cout <<".";break;//chemin
        case '1' : std::cout <<"#";break;//Mur
        case 'S' : std::cout <<"."; break;//sortie
        default : std::cout <<" ";break;
      }
    }
    std::cout<<std::endl;
  }
}
