#include "agent.hpp"
#include "heuristique.hpp"

Agent::Agent(Noeud* noeudCourant, std::string heuristique,Graphe* graphe){
	this->noeudCourant = noeudCourant;
	this->heuristique = getHeuristique(heuristique);
	this->graphe = graphe;
}

void Agent::bouger(){
	Noeud * prochainNoeud = heuristique(this->noeudCourant,this->noeudPrecedent,this->graphe->sortie);
	this->noeudPrecedent = this->noeudCourant;
	this->noeudCourant = prochainNoeud;
}