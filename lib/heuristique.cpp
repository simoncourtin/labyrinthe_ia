#include "heuristique.hpp"
#include <math.h>

Heuristique getHeuristique(std::string heuristique){
	if(heuristique == "A*"){
		return AStar;
	}
	else if(heuristique == "BestFirst"){
		return BestFirst;
	}
	else if(heuristique ==  "CoutUniforme"){
		return CoutUniforme;
	}
}

Noeud* AStar(Noeud* courant, Noeud* precedent, Noeud* sortie){
	std::vector<Noeud*> voisins = courant->voisin;
	float distance = 1000000;
	Noeud * voisin;
	int modification = 0;
	for(int i = 0; i<voisins.size(); i++){
		if(voisins[i] != precedent){
			float deltaX = sortie->x - voisins[i]->x;
			float deltaY = sortie->y - voisins[i]->y;
			float distanceVoisin = sqrt(deltaX * deltaX + deltaY*deltaY);

			if( distance > distanceVoisin ){
				distance = distanceVoisin;
				voisin = voisins[i];
				modification++;
			}
		}
	}
	if (modification == 0){
		return precedent;
	}
	return voisin;
}

Noeud* BestFirst(Noeud* courant, Noeud* precedent, Noeud* sortie){

}

Noeud* CoutUniforme(Noeud* courant, Noeud* precedent, Noeud* sortie){

}