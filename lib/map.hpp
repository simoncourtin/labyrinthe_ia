#ifndef MAP
#define MAP

#include <iostream>
#include <fstream>
#include <vector>
#include <string.h>

class Map{
	private :
		//variable
		std::vector<std::string> string_map;
		std::string fichier_map;
		int height;
		int width;
		float poidss;
		//function

	public :
		//function
		Map(std::string fichier_map);
		void AfficherConsole();
		std::vector<std::string>  lireFichierMap(std::string nom_fichier);

		//getter
		int getHeight(){return this->height;}
		int getWidth(){return this->width;}
		std::vector<std::string> getStringMap(){return this->string_map;}
		std::string getFichierMap(){return this->fichier_map;}

		//setter
		void setHeight(int height){this->height = height;}
		void setWidth(int width){this->width = width;}
		void setStringMap(std::vector<std::string> string_map){this->string_map = string_map;}
		void setFichierMap(std::string fichier_map){this->fichier_map = fichier_map;}

	};
#endif
