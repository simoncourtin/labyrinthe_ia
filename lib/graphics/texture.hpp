#ifndef TEXTURE_H
#define TEXTURE_H

#include <SFML/Graphics.hpp>
#include <iostream>

class Texture: public sf::Texture{
	private:
		std::string filePath;
		std::string name;
		int width,height;

	public:
		Texture();
		Texture(std::string filePath, std::string name, int width, int height);

		void createTexture();
		//getter
		std::string getFilePath(){return this->filePath;}
		std::string getName(){return this->name;}

		//setter
		void setFilePath(std::string filePath){this->filePath = filePath;}
		void setName(std::string name){this->name = name;}
};

#endif //TEXTURE_H
