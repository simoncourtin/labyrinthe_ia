#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>

#include "labyrintheMap.hpp"
#include "player.hpp"
#include "objectRender.hpp"


int main()
{
    std::vector<ObjectRender*> objets;

    LabyrintheMap lmap = LabyrintheMap("src/labyrinthe1.map",32);
    Player player = Player(1,1);
    objets.push_back(&lmap);
    objets.push_back(&player);

    //construction de la fenetre
    sf::RenderWindow window(sf::VideoMode(lmap.getWidth(), lmap.getHeight()), "Labyrinthe");

    int posx = 0;
    //boucle de la fenetre
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }


        player.setPosition(posx++,1);
        //clear screen
        window.clear();

        //affichage de tous les objets de la scene
        for(int i = 0; i < objets.size(); ++i){
          objets.at(i)->render(window);
        }
        //affichage
        window.display();

        //
        sf::sleep(sf::seconds(1.0));
    }
    return 0;
}
