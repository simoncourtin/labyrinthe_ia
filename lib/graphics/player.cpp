#include "player.hpp"

Player::Player(){
  Player(0,0,32);
}

Player::Player(int x, int y, int size){
  this->spriteSize = size;
  this->texture = new Texture("src/textures/diamond_chestplate.png","chestplate",32,32);
  this->sprite = new Sprite(this->texture,"player",x*size,y*size);
  float scale = (float)(size) / (float)(this->texture->getSize().x);
  this->sprite->setScale(scale,scale);
}

void Player::render(sf::RenderWindow &w){
	w.draw(*this->sprite);
}
