#ifndef PLAYER_H
#define PLAYER_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include "sprite.hpp"
#include "texture.hpp"
#include "objectRender.hpp"

class Player: public ObjectRender {

	private:
		Texture * texture;
		Sprite * sprite;
		int spriteSize;
		int x;
		int y;

	public:
		Player();
		Player(int x, int y, int size);
		virtual void render(sf::RenderWindow &w);
		virtual void init(){}
		virtual void update(){};

		//getter
		int getX(){return this->x;}
		int getY(){return this->y;}

		//setter
		void setPosition(int x, int y){
			this->x = x*this->spriteSize;
			this->y = y*this->spriteSize;
			this->sprite->setPosition(x*this->spriteSize,y*this->spriteSize);
		};
};

#endif //PLAYER_H
