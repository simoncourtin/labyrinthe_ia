#ifndef OBJECT_RENDER_H
#define OBJECT_RENDER_H

#include <iostream>
#include <SFML/Graphics.hpp>

class ObjectRender{

	public:
		ObjectRender(){}

		virtual void init(){}
		virtual void render(sf::RenderWindow &w){}
		virtual void update(){}
};

#endif //OBJECT_RENDER_H
