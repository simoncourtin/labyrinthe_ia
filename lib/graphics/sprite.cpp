#include "sprite.hpp"

Sprite::Sprite(Texture * texture, std::string name){
  this->setText(texture);
  this->setName(name);
  this->setPositionXY(0,0);
}

Sprite::Sprite(Texture * texture, std::string name,int x, int y){
  this->setText(texture);
  this->setName(name);
  this->setPositionXY(x,y);
}
