#include "graphicNode.hpp"

GraphicNode::GraphicNode(){
  GraphicNode(0,0,sf::Color(0, 255, 0),32,new Texture("src/textures/dirt.png","node",32,32));
}

GraphicNode::GraphicNode(int x, int y,sf::Color color,int size,Texture * t){
  this->texture = t;
  this->sprite = new Sprite(this->texture,"noeudOuvert",x*size,y*size);
  this->sprite->setColor(color);
  float scale = (float)(size) / (float)(this->texture->getSize().x);
  this->sprite->setScale(scale,scale);
}

void GraphicNode::render(sf::RenderWindow &w){
	w.draw(*this->sprite);
}
