#include "labyrintheMap.hpp"
#include <iostream>

LabyrintheMap::LabyrintheMap(std::string fichier_map, int spriteSize)
:GraphicMap(fichier_map, spriteSize){
	createTexture();
	createGraphicLabyrinthe();
}

void LabyrintheMap::createTexture(){

	//textures du labyrinthe
	Texture cobble = Texture("src/textures/cobblestone.png","cobble",32,32);
	Texture dirt = Texture("src/textures/dirt.png","dirt",32,32);
	Texture trapdoor = Texture("src/textures/trapdoor.png","trapdoor",32,32);

	//ajout dans la liste de texture
	this->textures.push_back(cobble);
	this->textures.push_back(dirt);
	this->textures.push_back(trapdoor);
}

Texture * LabyrintheMap::getTexture(std::string name){
	for (int i = 0; i < this->textures.size(); i++) {
		if(this->textures.at(i).getName() == name){
			return &this->textures.at(i);
		}
	}
	return new Texture();
}

void LabyrintheMap::createGraphicLabyrinthe(){
	//parcours de la martice fichier
	Sprite * tmpSprite;
	float scale;
	for(int i = 0; i < this->map.getStringMap().size(); i++){
		for(int j = 0 ; j < this->map.getStringMap().at(i).size(); j++){
			//selon l'element
			switch(this->map.getStringMap().at(i).at(j)) {
				case '0' :tmpSprite = new Sprite(getTexture("dirt"), "chemin",this->spriteSize * j,this->spriteSize * i);
									break;//chemin
				case '1' :tmpSprite = new Sprite(getTexture("cobble"), "wall" ,this->spriteSize * j,this->spriteSize * i);
									break;//Mur
				case 'S' :tmpSprite = new Sprite(getTexture("trapdoor"),"sortie",this->spriteSize * j,this->spriteSize * i);
									break;//sortie
				default :	tmpSprite = new Sprite(getTexture("cobble"), "wall" ,this->spriteSize * j,this->spriteSize * i);
								break;//chemin
			}
			scale = (float)(this->spriteSize) / (float)(tmpSprite->getTexture()->getSize().x);
		  tmpSprite->setScale(scale,scale);
			this->sprites.push_back(*tmpSprite);
		}
	}
}
