#include "graphicMap.hpp"

GraphicMap::GraphicMap(std::string fichier_map, int spriteSize):map(fichier_map){
	this->spriteSize = spriteSize;
	this->width = map.getWidth() * this->spriteSize;
	this->height = map.getHeight() * this->spriteSize;

}

void GraphicMap::draw(sf::RenderWindow &w){
	for (int i = 0; i < this->sprites.size(); i++) {
		w.draw(this->sprites.at(i));
	}
}

void GraphicMap::render(sf::RenderWindow &w){
	this->draw(w);
}
