#include "texture.hpp"

Texture::Texture(){
}

Texture::Texture(std::string filePath, std::string name, int width, int height){
  this->filePath = filePath;
  this->name = name;
  this->width = width;
  this->height = height;
  this->loadFromFile(filePath, sf::IntRect(0, 0, width, height));
}

void Texture::createTexture(){
  this->loadFromFile(this->filePath, sf::IntRect(0, 0, this->width, this->height));
}
