#ifndef LABYRINTHE_MAP
#define LABYRINTHE_MAP

#include <SFML/Graphics.hpp>
#include <string.h>
#include "graphicMap.hpp"
#include "texture.hpp"

class LabyrintheMap:public GraphicMap {
	private :
		std::vector<Texture> textures;
		Texture *getTexture(std::string name);
		void createTexture();
		void createGraphicLabyrinthe();
	public :
		LabyrintheMap(std::string fichier_map, int spriteSize = 32);

	};
#endif
