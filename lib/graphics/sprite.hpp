#ifndef SPRITE_H
#define SPRITE_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include "texture.hpp"

class Sprite: public sf::Sprite {
	private:
		Texture * texture;
		std::string name;
		int position_x;
		int position_y;

	public:
		Sprite();
    Sprite(Texture * texture, std::string name);
		Sprite(Texture * texture, std::string name,int x, int y);

		//getter
		Texture * getTexture(){return this->texture;}
		std::string getName(){return this->name;}
		int getPositionX(){return this->position_x;}
		int getPositionY(){return this->position_y;}

		//setter
		void setText(Texture * t){this->setTexture(*t);this->texture = t;}
		void setName(std::string name){this->name = name;}
		void setPositionXY(int x, int y){
			this->position_x = x;
			this->position_y = y;
			this->setPosition(x,y);
		};
};

#endif //SPRITE_H
