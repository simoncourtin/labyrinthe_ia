#ifndef MAP_GRAPHIC
#define MAP_GRAPHIC

#include <SFML/Graphics.hpp>
#include <string.h>
#include "../map.hpp"
#include "sprite.hpp"
#include "objectRender.hpp"

class GraphicMap : public ObjectRender{
	protected :

    std::vector<Sprite> sprites;
    Map map;
		int width;
		int height;
		int spriteSize;

	public :
		//function
    GraphicMap(std::string fichier_map, int spriteSize = 32);
		void draw(sf::RenderWindow &w);
		virtual void render(sf::RenderWindow &w);
		virtual void init(){}
		virtual void update(){};


    //getters
    Map getMap(){return this->map;};
		std::vector<Sprite> getSprites(){return this->sprites;};
		int getHeight(){return this->height;};
		int getWidth(){return this->width;};

    //setters
   	void setMap(Map m){this->map = m;};
		void setSprites(std::vector<Sprite> sprites){this->sprites = sprites;};
	};
#endif
