#include "parcourgraphe.hpp"
#include <iostream>
#include <vector>
#include <math.h>
#include <algorithm>


int comparerNoeud(Noeud *n1, Noeud *n2){
   if(n1->heuristique < n2->heuristique) {
       return 1;
   }
   else if (n1->heuristique  == n2->heuristique){
       return 0;
   }
   else{
       return -1;
   }
}

void Parcourgraphe::initParcour(std::string heuristique, Graphe G, Noeud* Sortie,  Noeud* Depart){
	this->graphe = G;
	this->sortie = Sortie;
	this->depart = Depart;
	this->heuristique = heuristique;
	openList.clear();
	closedList.clear();
	chemin.clear();
	for(int i = 0; i < graphe.graphe.size(); i++){
		graphe.graphe.at(i)->cout = 1000000;
	}

	this->depart->cout = 0;
	openList.push_back(this->depart);
}

int Parcourgraphe::pas(){
	if (openList.size() >0){
		Noeud* u = openList.at(0);

		if (u->x == this->sortie->x && u->y == this->sortie->y){
		   	openList.erase(std::remove(openList.begin(), openList.end(), u), openList.end());
			closedList.push_back(u);
		   	return 0;
		}
		for(int i = 0; i < u->voisin.size(); i++){
				Noeud* v = u->voisin.at(i);
				std::vector<Noeud*>::iterator it;
				std::vector<Noeud*>::iterator it2;
				it = find (closedList.begin(), closedList.end(), v);
				it2 = find (openList.begin(), openList.end(), v);
		    if( (it != closedList.end() && v->cout < u->cout) || (it2 != openList.end() && v->cout < u->cout)){

		    }
		    else{
		        v->cout = u->cout +1;
		        if(heuristique == "A*"){
					v->heuristique = v->cout + sqrt((this->sortie->x - v->x )* (this->sortie->x - v->x) + (this->sortie->y - v->y)*(this->sortie->y - v->y));
				}
				else if(heuristique == "BestFirst"){
					v->heuristique = sqrt((this->sortie->x - v->x )* (this->sortie->x - v->x) + (this->sortie->y - v->y)*(this->sortie->y - v->y));
				}
				else if(heuristique ==  "CoutUniforme"){
					v->heuristique = v->cout;
				}
		        openList.push_back(v);
		    }
		}
		openList.erase(std::remove(openList.begin(), openList.end(), u), openList.end());
		closedList.push_back(u);
		std::sort (openList.begin(), openList.end(), comparerNoeud);
	}
	else{
		return -1;
	}
	return 1;
}

std::vector<Noeud*> Parcourgraphe::AEtoile(Graphe graphe, Noeud* sortie,  Noeud* depart){
	initParcour("A*", graphe, sortie, depart);
	int status = 1;
	while (status == 1){
       status = this->pas();
    }
    if (status == 0){
    	chemin = reconstituerChemin(this->sortie);
    }
    return chemin;
}

std::vector<Noeud*> Parcourgraphe::BestFirst(Graphe graphe, Noeud* sortie,  Noeud* depart){
	initParcour("BestFirst", graphe, sortie, depart);
	int status = 1;
	while (status == 1){
       status = this->pas();
    }
    if (status == 0){
    	chemin = reconstituerChemin(this->sortie);
    }
    return chemin;
}

std::vector<Noeud*> Parcourgraphe::CoutUniforme(Graphe graphe, Noeud* sortie,  Noeud* depart){
	initParcour("CoutUniforme", graphe, sortie, depart);
	int status = 1;
	while (status == 1){
       status = this->pas();
    }
    if (status == 0){
    	chemin = reconstituerChemin(this->sortie);
    }
    return chemin;
}


std::vector<Noeud*> Parcourgraphe::reconstituerChemin(Noeud* u){
	Noeud* courant = u;
	std::vector<Noeud*>::iterator it;
	it = chemin.begin();
	it = chemin.insert ( it , u );

	while (courant->cout != 0){
		Noeud* precedent = courant->voisin.at(0);
		for (int i = 0; i < courant->voisin.size(); i++){
			if (courant->voisin.at(i)->cout < courant->cout){
				precedent = courant->voisin.at(i);
			}
		}
		it = chemin.begin();
		it = chemin.insert ( it , precedent);
		courant = precedent;
	}
	return chemin;
}
