#ifndef AGENT
#define AGENT

#include "graphe.hpp"
#include "heuristique.hpp"

class Agent{
	public:
		Agent(Noeud* noeudCourant, std::string heuristique, Graphe* graphe);
		void bouger();
		Noeud* noeudCourant;
		Noeud* noeudPrecedent;
	private:
		Noeud* (*heuristique)(Noeud*, Noeud*,Noeud*);
		Graphe* graphe;
		Graphe* chemin;
		std::vector<Agent*> fils;
};

#endif