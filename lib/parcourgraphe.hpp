#ifndef PARCOURGRAPHE
#define PARCOURGRAPHE

#include "noeud.hpp"
#include "graphe.hpp"
class Parcourgraphe{
	public:	

		std::vector<Noeud*>  AEtoile(Graphe G, Noeud* Sortie, Noeud* depart);
		std::vector<Noeud*>  BestFirst(Graphe G, Noeud* sortie,  Noeud* depart);
		std::vector<Noeud*>  CoutUniforme(Graphe G, Noeud* sortie,  Noeud* depart);

		void initParcour(std::string heuristique, Graphe G, Noeud* Sortie,  Noeud* Depart);
		int pas();
		std::vector<Noeud*> reconstituerChemin(Noeud* u);

		std::vector<Noeud*> closedList;
   		std::vector<Noeud*> openList;
   		std::vector<Noeud*> chemin;
   	private:
   		Graphe graphe;
   		Noeud* sortie;  
   		Noeud* depart;
   		std::string heuristique;
};
int comparerNoeud(Noeud *n1, Noeud *n2);

#endif