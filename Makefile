CC=g++ --std=c++11
OUT=bin/ex1.bin

UNAME=$(shell uname)


SOURCE_FILES=src/main.cpp

LIBS=lib/noeud.cpp lib/graphe.cpp lib/agent.cpp lib/heuristique.cpp lib/map.cpp lib/parcourgraphe.cpp lib/graphics/sprite.cpp lib/graphics/texture.cpp lib/graphics/graphicMap.cpp lib/graphics/labyrintheMap.cpp lib/graphics/player.cpp lib/graphics/graphicNode.cpp
SOURCES=${SOURCE_FILES} ${LIBS}
OBJECTS=$(SOURCES:.c=.o)

all: $(OBJECTS)
	${CC} ${OBJECTS} ${CCFLAGS} -o ${OUT} -lsfml-graphics -lsfml-window -lsfml-system

%.o: %.c
	${CC} ${CCFLAGS} -cpp $^ -o $@

clean:
	rm -f *.o

mrproper: clean
	rm -f ${OUT}
